package j.jarocha_linear_search;

import java.util.List;

/**
 * Created by RENT on 2017-08-18.
 */
public class LinearSearch<T extends Comparable<T>> extends Search<T> {

    @Override
    public boolean doSearch(List<T> list, T searchElement) {
        for (int i = 0; i < list.size(); i++) {
            T car = list.get(i);
            if (car.compareTo(searchElement) == 0) {
                System.out.println("Mamy auto " + car + " na liście.");
                return true;
            }
        }
        return false;
    }
}
