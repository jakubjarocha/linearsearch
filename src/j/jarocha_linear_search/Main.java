package j.jarocha_linear_search;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by RENT on 2017-08-18.
 */
public class Main {

    public static void main(String[] args) {

        Car car1 = new Car("Audi", "A3");
        Car car2 = new Car("Bugatti", "Veyron");
        Car car3 = new Car("Alfa Romeo", "Giulietta");
        Car car4 = new Car("Ferrari", "Enzo");
        Car car5 = new Car("Lamborghini", "Murcielago");

        List<Car> listOfCars = new ArrayList<>();

        listOfCars.add(car1);
        listOfCars.add(car2);
        listOfCars.add(car3);
        listOfCars.add(car4);

        for (int i = 0; i < listOfCars.size(); i++) {
            Car car = listOfCars.get(i);
            if (car.compareTo(car3) == 0) {
                System.out.println("Mamy auto " + car + " na liście.");
                return;
            }
        }
        System.out.println("Nie mamy auta na liście.");

        BinarySearch binarySearch = new BinarySearch();
        LinearSearch linearSearch = new LinearSearch();

        Search<Car> search = new LinearSearch<Car>();
        search.doSearch(listOfCars, car5);
    }
}
