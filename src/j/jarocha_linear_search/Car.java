package j.jarocha_linear_search;

/**
 * Created by RENT on 2017-08-18.
 */
public class Car implements Comparable<Car> {

    protected String mark;
    protected String model;

    public Car() {
    }

    public Car(String mark, String model) {
        this.mark = mark;
        this.model = model;
    }

    public String getMark() {
        return mark;
    }

    public void setMark(String mark) {
        this.mark = mark;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }


    @Override
    public int compareTo(Car car) {

        int result = -1;



        if (getMark() == car.getMark() && getModel() == car.getModel()) {
                result = 0;
            }
        return result;
    }

    public String toString() {
        return "" + mark + " " + model;
    }
}
